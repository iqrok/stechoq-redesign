<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package stechoq-redesign
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function stechoq_redesign_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'stechoq_redesign_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function stechoq_redesign_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'stechoq_redesign_pingback_header' );

/** widgets */
register_sidebar(
	array(
		'name'          => esc_html__( 'Footer Menu Logo' ),
		'id'            => 'footer-menu-logo',
		'description'   => esc_html__( 'Add Footer Logo widgets here.' ),
		'before_widget' => '<div id="%1$s" class="footer-widget %2$s dynamic-classes">',
		'after_widget'  => '</div><!-- .footer-widget -->',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	)
);

register_sidebar(
	array(
		'name'          => esc_html__( 'Footer Menu Left' ),
		'id'            => 'footer-menu-left',
		'description'   => esc_html__( 'Add Footer Left widgets here.' ),
		'before_widget' => '<div id="%1$s" class="footer-widget %2$s dynamic-classes">',
		'after_widget'  => '</div><!-- .footer-widget -->',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	)
);

register_sidebar(
	array(
		'name'          => esc_html__( 'Footer Menu Middle' ),
		'id'            => 'footer-menu-middle',
		'description'   => esc_html__( 'Add Footer Middle widgets here.' ),
		'before_widget' => '<div id="%1$s" class="footer-widget %2$s dynamic-classes">',
		'after_widget'  => '</div><!-- .footer-widget -->',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	)
);

register_sidebar(
	array(
		'name'          => esc_html__( 'Footer Menu Right' ),
		'id'            => 'footer-menu-right',
		'description'   => esc_html__( 'Add Footer Right widgets here.' ),
		'before_widget' => '<div id="%1$s" class="footer-widget %2$s dynamic-classes">',
		'after_widget'  => '</div><!-- .footer-widget -->',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	)
);

/**
 * /**
 * The [get_product] shortcode.
 *
 * Get all products and display them
 *
 * @param array  $atts    Shortcode attributes. Default empty.
 * @param string $content Shortcode content. Default null.
 * @param string $tag     Shortcode tag (name). Default empty.
 * @return string Shortcode output.
 */
function stechoq_redesign_get_product( $atts = [], $content = null, $tag = '' ) {
	global $wpdb;

    // normalize attribute keys, lowercase
    $atts = array_change_key_case( (array) $atts, CASE_LOWER );

    // override default attributes with user attributes
    $attributes = shortcode_atts(
        array(
            'slug' => 'product-',
        ), $atts, $tag
    );

	$sql = "SELECT ID id, post_title title, post_name name FROM ". $wpdb->prefix ."posts WHERE post_type = \"page\" AND post_name LIKE \"". $attributes['slug'] ."%\" ORDER BY ID ASC;";
	$products = $wpdb->get_results($sql);

	$html = '<div class="container-fluid w-75"><div class="our-product-container">';

    foreach($products as $key => $val){
		$thumbnail = wp_get_attachment_url( get_post_thumbnail_id($val->id), 'thumbnail');
		$permalink = get_permalink( get_page_by_path( $val->name ) );

		$out = '<a href="'. $permalink .'">';
		$out .= '<div class="our-product">';
		$out .= '<img src="'. $thumbnail .'" alt="'. $val->name .'-img">';
		$out .= '<div class="layer-transparent"></div>';
		$out .= '<h1>'. $val->title .'</h1>';
		$out .= '</div>';
		$out .= '</a>';

		$html .= $out;
	}

	$html .= '</div></div>';

	return $html;
}

/**
 * /**
 * The [get_page_uri] shortcode.
 *
 * Get page URI by slug
 *
 * @param array  $atts    Shortcode attributes. Default empty.
 * @param string $content Shortcode content. Default null.
 * @param string $tag     Shortcode tag (name). Default empty.
 * @return string Shortcode output.
 */
function stechoq_redesign_get_page_uri( $atts = [], $content = null, $tag = '' ) {
	global $wpdb;

    // normalize attribute keys, lowercase
    $atts = array_change_key_case( (array) $atts, CASE_LOWER );

    // override default attributes with user attributes
    $attributes = shortcode_atts(
        array(
            'slug' => 'home',
        ), $atts, $tag
    );

	return get_permalink( get_page_by_path( $attributes['slug'] ) );
}

/**
 * Central location to create all shortcodes.
 */
function stechoq_redesign_shortcodes_init() {
    add_shortcode( 'get_product', 'stechoq_redesign_get_product' );
    add_shortcode( 'get_page_uri', 'stechoq_redesign_get_page_uri' );
}

add_action( 'init', 'stechoq_redesign_shortcodes_init' );
