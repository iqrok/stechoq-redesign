const videos = [];
const tag = document.createElement("script");
const firstScriptTag = document.getElementsByTagName("script")[0];

tag.src = "https://www.youtube.com/iframe_api";
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// YouTube wants this function, don't rename it
function onYouTubeIframeAPIReady() {
	const slides = Array.from(document.querySelectorAll(".carousel-item"));
	slides.forEach((slide, index) => {
		// does this slide have a video?
		const video = slide.querySelector(".video-player");
		if (video && video.dataset) {
			const player = createPlayer({
				id: video.id,
				videoId: video.dataset.videoId,
			});
			videos.push({ player, index });
		}
	});
}

function createPlayer(playerInfo) {
	return new YT.Player(playerInfo.id, {
		videoId: playerInfo.videoId,
		playerVars: {
			showinfo: 0,
			controls: 0,
			loop: 1,
			playlist: playerInfo.videoId,
		},
	});
}

function theBigPause() {
	videos.map((video) => video.player.pauseVideo());
}

$(function () {
	const _carousel = $('.carousel');
	_carousel.carousel();
	const start = _carousel.find('.active').attr('data-interval');
	let t = setTimeout(() => {
			_carousel.carousel({interval: 1000});
		}, start-1000);
	$(".carousel").on("slide.bs.carousel", function (e) {
			clearTimeout(t);
			const duration = $(this).find('.active').attr('data-interval');

			_carousel.carousel('pause');
			t = setTimeout(() => {
					_carousel.carousel();
				}, duration-1000);

			theBigPause();
			const next = $(e.relatedTarget).index();
			const video = videos.filter((v) => v.index === next)[0];
			if (video) {
				video.player.playVideo();
			}
		});
});
