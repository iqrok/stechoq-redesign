<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package stechoq-redesign
 */

get_header();
?>

	<main id="primary" class="site-main container my-5 py-5" style="min-height: 50vh;">

		<section class="error-404 not-found">
			<header class="page-header row align-items-center">
				<div class="col text-center text-lg-right">
					<h1 class="page-title display-1 my-1">
						<?php esc_html_e( '404', 'stechoq-redesign' ); ?>
					</h1>
				</div>

				<div class="w-100 hide-in-full"></div>

				<div class="col text-center text-lg-left">
					<div class="row">
						<div class="col-12">
							<h2><?php esc_html_e('Page not Found', 'stechoq-redesign') ?></h2>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<h5><?php esc_html_e('The page you\'re looking for doesn\'t exist!', 'stechoq-redesign') ?></h5>
						</div>
					</div>

				</div>
			</header><!-- .page-header -->

			<div class="page-content row">
				<div class="col text-center">
					<a class="btn btn-primary btn-lg" href="<?php echo get_home_url() ?>">
						<i class="fa-solid fa-home"></i> Home
					</a>
				</div>
			</div><!-- .page-content -->
		</section><!-- .error-404 -->

	</main><!-- #main -->

<?php
//~ get_footer();
