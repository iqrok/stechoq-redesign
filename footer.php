<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package stechoq-redesign
 */

?>

	<footer id="colophon" class="site-footer py-4">
		<div class="container-fluid">
			<div class="row px-5">
				<div class="col-lg-4 col-sm-12">
					<?php
					if ( is_active_sidebar( 'footer-menu-logo' ) ){
						dynamic_sidebar( 'footer-menu-logo' );
					}
					?>
				</div>

				<div class="col-lg-3 hide-in-mobile widget-area">
				<?php
				if ( is_active_sidebar( 'footer-menu-left' ) ){
					dynamic_sidebar( 'footer-menu-left' );
				}
				?>
				</div>

				<div class="col-lg-3 hide-in-mobile widget-area">
				<?php
				if ( is_active_sidebar( 'footer-menu-middle' ) ){
					dynamic_sidebar( 'footer-menu-middle' );
				}
				?>
				</div>

				<div class="col-lg-2 col-sm-12 widget-area mt-sm-4 mt-lg-0">
				<?php
				if ( is_active_sidebar( 'footer-menu-right' ) ){
					dynamic_sidebar( 'footer-menu-right' );
				}
				?>
				</div>
			</div>

			<div class="row">
				<div class="col">
					<div class="site-info mt-3">
						<div class="mt-3 font-weight-bold">
						<?php
						/* translators: 1: Theme name, 2: Theme author. */
						printf( esc_html__( 'STECHOQ © 2022', 'stechoq-redesign' ) );
						?>
						</div>
					</div><!-- .site-info -->
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
