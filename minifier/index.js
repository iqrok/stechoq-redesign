const fs = require('fs');
const path = require('path');

const UglifyJS = require('uglify-js');
const CleanCSS = require('clean-css');

const cleanCSS = new CleanCSS({
	level: {
		2: {
			all: true, // sets all values to `false`
		}
	}
});

/**
 *	Gets all files from directory an its sub-directories
 *	@private
 *	@param {string} __path - directory path
 *	@param {string} __extension - file extension to get
 *	@returns {Object[]} list of files
 */
async function getFiles(__path, __extension, __recursively = true) {
	const files = [];
	const __readdir = await fs.promises.readdir(__path);

	for (const file of __readdir) {
		const stats = await fs.promises.stat(__path + path.sep + file);

		if (stats.isFile() && path.extname(file) === __extension) {
			files.push(file);
		} else if (stats.isDirectory() && __recursively) {
			// if file is a directory, recursively get all files inside it and add them into object
			const tmp = await getFiles(path.resolve(__path, file), __extension);

			for (const item of tmp) {
				const __file = item instanceof Object ?
					{ ...item } :
					{
						path: (__path + path.sep + path.basename(file, path.extname(file)) + path.sep + item).replace(__basedir + path.sep, ''),
						type: path.basename(file, path.extname(file))
					};

				files.push(__file);
			}
		}
	}

	return files;
}

global.__basedir = __dirname;

(async() => {
	const js = {
		dir: `${__basedir}/../js`,
		ext: '.js',
		get files(){
			return getFiles(this.dir, this.ext);
		},
	};

	const css = {
		dir: `${__basedir}/..`,
		ext: '.css',
		get files(){
			return getFiles(this.dir, this.ext, false);
		},
	};

	for(const JS of await js.files){
		const basename = path.basename(JS, js.ext);

		if(basename.includes('.min')){
			continue;
		}

		const codes = await fs.promises.readFile(`${js.dir}/${JS}`, 'utf8');
		const ugly = UglifyJS.minify(codes);

		if(!ugly.error){
			fs.promises.writeFile(`${js.dir}/${basename}.min${js.ext}`, ugly.code);
		}
	}

	for(const CSS of await css.files){
		const basename = path.basename(CSS, css.ext);

		if(basename.includes('.min')){
			continue;
		}

		const codes = await fs.promises.readFile(`${css.dir}/${CSS}`, 'utf8');
		const ugly = cleanCSS.minify(codes);

		if(ugly.styles){
			fs.promises.writeFile(`${css.dir}/${basename}.min${css.ext}`, ugly.styles);
		}
	}
})();
